const axios = require('axios')
const moment = require('moment')

async function index(req, res) {
    try {
        const {data} = await axios.get('http://localhost:3000/cars', {
            headers: {
                "Authorization": `Bearer ${req.cookies.access_token}`
            }
        })

        const user = await axios.get(`http://localhost:3000/users/${req.user.id}`)

        if (!user.data.status) {
            throw Error(user.data.messages)
        }
        
        if (!data.status) {
            throw Error(data.message)
        }

        const cars = data.data.map(car => {
            return {
                id: car.id,
                name: car.name,
                rent: car.rent,
                size: car.size,
                image: `http://localhost:3000/image/${car.image}`,
                createdAt: moment(`${car.createdAt}`).format('DD MMM YYYY, HH:mm'),
                updatedAt: moment(`${car.updatedAt}`).format('DD MMM YYYY, HH:mm'),
            }
        })

        res.render('Admin', {
            title: 'Binar Car Rental | Admin',
            content: 'index',
            action: (Object.keys(req.query).length > 0) ? req.query.action : false,
            data: {
                data: cars,
                user: user.data.data,
                meta: data.meta,
            }
        })

    }
    catch(err) {
        res.status(500).render('OnError', {
            status: 500,
            message: err.message
        })
    }
}

async function addCar(req, res) {
    try {
        const user = await axios.get(`http://localhost:3000/users/${req.user.id}`, {
            headers: {
                "Authorization": `Bearer ${req.cookies.access_token}`
            }
        })
    
        if (!user.data.status) {
            throw Error(user.data.messages)
        }
    
        res.render('Admin', {
            title: 'Binar Car Rental | Admin',
            content: 'addCar',
            data: {
                user: user.data.data
            }
        })
    } 
    catch(err) {
        res.status(500).render('OnError', {
            status: 500,
            message: err.message
        })
    }

}

async function getCar(req, res) {
    let data = []

    await axios.get(`http://localhost:3000/car/${req.params.id}`, {
        headers: {
            "Authorization": `Bearer ${req.cookies.access_token}`
        }
    })
    .then(response => {
        data = response.data
    })

    const user = await axios.get(`http://localhost:3000/users/${req.user.id}`)
    if (!user.data.status) {
        throw Error(user.data.messages)
    }

    res.render('Admin', {
        title: 'Binar Car Rental | Admin',
        content: 'updateCar',
        data: {
            data: data,
            user: user.data.data
        }
    })
}

module.exports = {index, addCar, getCar}