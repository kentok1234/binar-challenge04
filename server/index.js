const express = require('express')
const bodyParser = require('body-parser')
const cookie = require('cookie-parser')
const constroller = require('../controllers')
const app = express()
const port = 8080

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(cookie())
app.use(constroller)
app.use((req, res, next) => {
  res.status(404).render('OnError', {
    status: 404,
    message: 'Halaman tidak ditemukan'
  })
})
app.use((req, res, next) => {
  res.status(500).render('OnError', {
    status: 500,
    message: "Terjadi kesalahan pada server"
  })
})

app.set('view engine', 'ejs')

app.listen(port, () => {
  console.log(`Server running on port ${port}`)
})